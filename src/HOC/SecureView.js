import React, { useEffect } from "react";
import { localServ } from "../Services/localService";

export default function SecureView({ children }) {
  let userLocal = localServ.user.get().user;
  useEffect(() => {
    if (!userLocal) {
      window.location.href = "/login";
      alert("Bạn chưa đăng nhập!!!");
    } else {
      let loaiND = userLocal.role;
      if (loaiND == "USER") {
        window.location.href = "/login";
        alert(
          "Bạn không đủ quyền truy cập. Xin hãy chọn tài khoản Quản Trị!!!"
        );
        localServ.user.remove();
      }
    }
  }, []);

  return <div>{children}</div>;
}

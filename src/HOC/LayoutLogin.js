import HeaderPage from "../Page/HeaderPage/HeaderPage";

export default function LayoutLogin({ Component }) {
  return (
    <div>
      <HeaderPage />
      <div style={{ marginTop: "150px" }}>
        <Component />
      </div>
    </div>
  );
}

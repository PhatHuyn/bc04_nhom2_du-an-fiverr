import {
  DesktopOutlined,
  UserOutlined,
  SettingOutlined,
  FolderAddOutlined,
  SnippetsOutlined,
  OrderedListOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import HeaderPage from "../Page/HeaderPage/HeaderPage";
const { Content, Sider } = Layout;

function MenuSider({ Children }) {
  const navigate = useNavigate();
  return (
    <div>
      <Layout style={{ minHeight: "100vh" }}>
        <Sider collapsible>
          <div className="logo" />
          <Menu
            mode="inline"
            theme="dark"
            onClick={({ key }) => {
              navigate(key);
            }}
            items={[
              {
                label: "Quản lý Người dùng",
                icon: <UserOutlined />,
                key: "/admin/UserManage",
              },
              {
                label: "Quản lý công việc",
                icon: <SettingOutlined />,
                key: "/admin/WorkManage",
              },
              {
                label: "Quản lý loại công việc",
                icon: <SnippetsOutlined />,
                children: [
                  {
                    label: "Loại công việc",
                    key: "/admin/TypeOfWorkManage",
                    icon: <DesktopOutlined />,
                  },
                  {
                    label: "Chi tiết loại công việc",
                    key: "/admin/JobTypeDetailManage",
                    icon: <FolderAddOutlined />,
                  },
                ],
              },
              {
                label: "Quản lý dịch vụ",
                icon: <OrderedListOutlined />,
                children: [
                  {
                    label: "Quản lý Thuê công việc",
                    key: "/admin/HireAJobManage",
                    icon: <DesktopOutlined />,
                  },
                  {
                    label: "Quản lý Bình luận",
                    key: "/admin/CommentManage",
                    icon: <FolderAddOutlined />,
                  },
                ],
              },
            ]}
          ></Menu>
        </Sider>

        <Layout className="site-layout">
          <Content
            style={{
              margin: "0 16px",
            }}
          >
            <div
              className="site-layout-background"
              style={{
                padding: 24,
                minHeight: 360,
              }}
            >
              {Children}
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
}

export default function LayoutAdmin({ Component }) {
  return (
    <div>
      <HeaderPage />
      <div style={{ marginTop: "90px" }}>
        <MenuSider Children={<Component />} />
      </div>
    </div>
  );
}

import { SET_LIST_JOBTYPEDETAIL } from "../constant/constantJobTypeDetail";

export const setListJobTypeDetail = (Value) => {
  return {
    type: SET_LIST_JOBTYPEDETAIL,
    payload: Value,
  };
};

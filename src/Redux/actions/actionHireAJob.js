import { SET_LIST_HIREAJOB } from "../constant/constantHireAJob";

export const setListHireAJob = (Value) => {
  return {
    type: SET_LIST_HIREAJOB,
    payload: Value,
  };
};

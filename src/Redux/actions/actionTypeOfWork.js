import { SET_LIST_TYPEOFWORK } from "../constant/constantTypeOfWork";

export const setListTypeOfWork = (Value) => {
  return {
    type: SET_LIST_TYPEOFWORK,
    payload: Value,
  };
};

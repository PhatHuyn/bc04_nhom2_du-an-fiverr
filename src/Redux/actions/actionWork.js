import { SET_LIST_WORK } from "../constant/constantWork";

export const setListWork = (Value) => {
  return {
    type: SET_LIST_WORK,
    payload: Value,
  };
};

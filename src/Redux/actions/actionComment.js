import { SET_LIST_COMMENT } from "../constant/constantComment";

export const setListComment = (Value) => {
  return {
    type: SET_LIST_COMMENT,
    payload: Value,
  };
};

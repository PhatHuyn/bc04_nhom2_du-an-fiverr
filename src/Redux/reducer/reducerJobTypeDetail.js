import { SET_LIST_JOBTYPEDETAIL } from "../constant/constantJobTypeDetail";

let initialState = {
  listJobTypeDetail: [],
};

export const jobtypedetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_JOBTYPEDETAIL:
      state.listJobTypeDetail = action.payload;
      // for (let i = 0; i <= state.listJobTypeDetail.length; i++) {
      //   console.log("state.listJobTypeDetail[i]", state.listJobTypeDetail[i]);
      // }
      return { ...state };
    default:
      return { ...state };
  }
};

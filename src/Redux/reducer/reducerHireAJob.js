import { SET_LIST_HIREAJOB } from "../constant/constantHireAJob";

let initialState = {
  listHireAJob: [],
};

export const hireajobReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_HIREAJOB:
      state.listHireAJob = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

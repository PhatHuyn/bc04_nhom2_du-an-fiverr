import { SET_LIST_TYPEOFWORK } from "../constant/constantTypeOfWork";

let initialState = {
  listTypeOfWork: [],
};

export const typeofworkReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_TYPEOFWORK:
      state.listTypeOfWork = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

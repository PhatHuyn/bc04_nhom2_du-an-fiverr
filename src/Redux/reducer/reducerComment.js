import { SET_LIST_COMMENT } from "../constant/constantComment";

let initialState = {
  listComment: [],
};

export const commentReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_COMMENT:
      state.listComment = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

import { combineReducers } from "redux";
import { userReducer } from "./reducerUser";
import { workReducer } from "./reducerWork";
import { typeofworkReducer } from "./reducerTypeOfWork";
import { jobtypedetailReducer } from "./reducerJobTypeDetail";
import { hireajobReducer } from "./reducerHireAJob";
import { commentReducer } from "./reducerComment";

export let rootReducer = combineReducers({
  userReducer,
  workReducer,
  typeofworkReducer,
  jobtypedetailReducer,
  hireajobReducer,
  commentReducer,
});

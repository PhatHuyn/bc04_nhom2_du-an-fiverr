import { SET_LIST_WORK } from "../constant/constantWork";

let initialState = {
  listWork: [],
};

export const workReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST_WORK:
      state.listWork = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

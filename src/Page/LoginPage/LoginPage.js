import { Button, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import bg_animate from "../../assets/bg_Login.json";
import { userServ } from "../../Services/userService";
import { setUserLogin } from "../../Redux/actions/actionUser";
import { localServ } from "../../Services/localService";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();

  const onFinish = (values) => {
    userServ
      .postLogin(values)
      .then((res) => {
        dispatch(setUserLogin(res.data.content.user));
        localServ.user.set(res.data.content);
        message.success("Đăng nhập thành công");
        navigate("/admin");
        window.location.reload();
      })
      .catch((err) => {
        message.error(err.response.data.content);
      });
  };

  const onFinishFailed = (errorInfo) => {
    message.error(errorInfo);
  };

  return (
    <div className="container mx-auto h-screen w-screen flex items-center justify-center  ">
      <div className="w-1/2 h-full">
        <Lottie animationData={bg_animate} />
      </div>
      <div className="w-1/2 h-full flex items-center justify-center ">
        <Form
          className=" w-full "
          layout="vertical"
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{}}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <h1 className="text-5xl">Đăng Nhập Fiverr</h1>
          <Form.Item
            label="Email:"
            name="email"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập email!!!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật Khẩu:"
            name="password"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu!!!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" className="mr-5">
              Đăng Nhập
            </Button>
            <NavLink to="/signup">
              <Button type="link">Đăng Ký</Button>
            </NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default LoginPage;

import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Table, Input } from "antd";
import { NavLink } from "react-router-dom";
import { commentService } from "../../Services/commentService";
import { setListComment } from "../../Redux/actions/actionComment";
import CommentAction from "./CommentAction";

export default function QuanLyComment() {
  const { Search } = Input;
  let dispatch = useDispatch();

  const arrComment = useSelector((state) => {
    return state.commentReducer.listComment;
  });

  useEffect(() => {
    commentService
      .getListComment()
      .then((res) => {
        let dataComment = res.data.content.map((item) => {
          return { ...item, action: <CommentAction item={item} /> };
        });
        dispatch(setListComment(dataComment));
      })
      .catch((err) => {});
  }, []);

  const data = arrComment;

  const columns = [
    {
      title: "ID ",
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => a.id - b.id,
      width: "5%",
    },
    {
      title: "Mã công việc",
      dataIndex: "maCongViec",
      key: "maCongViec",
      sorter: (a, b) => a.maCongViec - b.maCongViec,
      width: "8%",
    },
    {
      title: "Mã người bình luận",
      dataIndex: "maNguoiBinhLuan",
      key: "maNguoiBinhLuan",
      sorter: (a, b) => a.maNguoiBinhLuan - b.maNguoiBinhLuan,
      width: "8%",
    },
    {
      title: "Sao bình luận",
      dataIndex: "saoBinhLuan",
      key: "saoBinhLuan",
      sorter: (a, b) => a.saoBinhLuan - b.saoBinhLuan,
      width: "8%",
    },
    {
      title: "Ngày bình luận",
      dataIndex: "ngayBinhLuan",
      key: "ngayBinhLuan",
      width: "10%",
    },
    {
      title: "Nội dung bình luận",
      dataIndex: "noiDung",
      key: "noiDung",
      width: "15%",
    },
    {
      title: "Tên người bình luận",
      dataIndex: "tenNguoiBinhLuan",
      key: "tenNguoiBinhLuan",
      width: "15%",
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      width: "20%",
    },
  ];

  const onSearch = (value) => {
    if (value != "") {
      commentService
        .SearchCommentId(value)
        .then((res) => {
          let dataComment = res.data.content.map((item) => {
            return { ...item, action: <CommentAction item={item} /> };
          });
          dispatch(setListComment(dataComment));
        })
        .catch((err) => {});
    } else {
      commentService
        .getListComment()
        .then((res) => {
          let dataComment = res.data.content.map((item) => {
            return { ...item, action: <CommentAction item={item} /> };
          });
          dispatch(setListComment(dataComment));
        })
        .catch((err) => {});
    }
  };

  const onChange = (pagination, filters, sorter, extra) => {
    // console.log("params onChange Table", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h3 className="text-xl mb-5">Quản lý bình luận</h3>
      <NavLink to="/admin/CommentManage/AddComment">
        <Button type="primary" className="mb-5">
          Thêm bình luận
        </Button>
      </NavLink>
      <Search
        className="mb-5"
        placeholder="Nhập mã công việc muốn kiếm bình luận"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey={"id"}
      />
    </div>
  );
}

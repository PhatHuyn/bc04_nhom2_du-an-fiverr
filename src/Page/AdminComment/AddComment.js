import { Button, Form, Input, Radio, DatePicker, InputNumber } from "antd";
import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import { commentService } from "../../Services/commentService";

export default function AddComment() {
  const { TextArea } = Input;
  const [componentSize, setComponentSize] = useState("default");
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      id: 0,
      maCongViec: "",
      maNguoiBinhLuan: "",
      ngayBinhLuan: "",
      noiDung: "",
      saoBinhLuan: "",
    },
    validationSchema: Yup.object({
      maCongViec: Yup.string()
        .required("Không được để trống mã công việc!!!")
        .matches(/^[0-9]+$/, "Mã công việc không được nhập chữ, hay ký tự!!!"),
      maNguoiBinhLuan: Yup.string()
        .required("Không được để trống mã người bình luận!!!")
        .matches(
          /^[0-9]+$/,
          "Mã người bình luận không được nhập chữ, hay ký tự!!!"
        ),
      ngayBinhLuan: Yup.string().required("Xin hãy chọn Ngày bình luận!!!"),
      noiDung: Yup.string().required(
        "Không được để trống nội dung bình luận!!!"
      ),
      saoBinhLuan: Yup.string().required("Xin hãy chọn sao bình luận!!!"),
    }),
    onSubmit: (values) => {
      commentService
        .AddComment(values)
        .then((res) => {
          alert("Đã thêm bình luận thành công!!!");
          navigate("/admin/CommentManage");
        })
        .catch((err) => {
          alert(err.response.data.content);
        });
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeDataPicker = (values) => {
    let ngayBinhLuan = moment(values).format("DD/MM/YYYY");
    formik.setFieldValue("ngayBinhLuan", ngayBinhLuan);
  };

  const handleChangeInputNumber = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Thêm bình luận</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập mã công việc">
        <Input name="maCongViec" onChange={formik.handleChange} />
        {formik.errors.maCongViec && (
          <p className="text-red-500">{formik.errors.maCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập mã người bình luận">
        <Input name="maNguoiBinhLuan" onChange={formik.handleChange} />
        {formik.errors.maNguoiBinhLuan && (
          <p className="text-red-500">{formik.errors.maNguoiBinhLuan}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập ngày bình luận">
        <DatePicker
          className="w-1/2"
          format={"DD/MM/YYYY"}
          onChange={handleChangeDataPicker}
        />
        {formik.errors.ngayBinhLuan && (
          <p className="text-red-500">{formik.errors.ngayBinhLuan}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập nội dung">
        <TextArea
          rows={3}
          name="noiDung"
          onChange={formik.handleChange}
          placeholder="Vui lòng nhập nội dung bình luận"
        />
        {formik.errors.noiDung && (
          <p className="text-red-500">{formik.errors.noiDung}</p>
        )}
      </Form.Item>

      <Form.Item label="Sao bình luận">
        <InputNumber onChange={handleChangeInputNumber("saoBinhLuan")} />
        {formik.errors.saoBinhLuan && (
          <p className="text-red-500">{formik.errors.saoBinhLuan}</p>
        )}
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Thêm Bình Luận
        </Button>
      </Form.Item>
    </Form>
  );
}

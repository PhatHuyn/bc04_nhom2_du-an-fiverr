import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { setListComment } from "../../Redux/actions/actionComment";
import { commentService } from "../../Services/commentService";

export default function CommentAction({ item }) {
  let dispatch = useDispatch();
  return (
    <div>
      <NavLink to={`/admin/CommentManage/EditComment/${item.id}`}>
        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2">
          Sửa
        </button>
      </NavLink>
      <button
        onClick={() => {
          if (
            window.confirm(`Bạn có chắc muốn xoá bình luận thứ "${item.id}"`)
          ) {
            commentService
              .DeleteComment(item.id)
              .then((res) => {
                alert(`Bình luận thứ ${item.id} xóa thành Công!!!`);
                commentService
                  .getListComment()
                  .then((res) => {
                    let dataComment = res.data.content.map((item) => {
                      return { ...item, action: <CommentAction item={item} /> };
                    });
                    dispatch(setListComment(dataComment));
                  })
                  .catch((err) => {
                    // console.log("err", err);
                  });
              })
              .catch((err) => {
                alert(err.response.data.content);
              });
          }
        }}
        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2"
      >
        Xoá
      </button>
    </div>
  );
}

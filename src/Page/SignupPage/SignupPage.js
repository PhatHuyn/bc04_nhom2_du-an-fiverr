import { Button, DatePicker, Form, Input, message, Radio } from "antd";
import React from "react";
import { NavLink, useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import bg_animate from "../../assets/bg_Login.json";
import moment from "moment";
import { useFormik } from "formik";
import * as Yup from "yup";
import { userServ } from "../../Services/userService";

const SignupPage = () => {
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      id: 0,
      name: "",
      email: "",
      password: "",
      phone: "",
      birthday: "",
      gender: "",
      role: "",
      skill: "",
      certification: "",
    },
    validationSchema: Yup.object({
      name: Yup.string().required("không được để trống Name!!!"),
      email: Yup.string().required("Không được để trống Email!!!"),
      password: Yup.string().required("không được để trống PassWord!!!"),
      phone: Yup.string()
        .required("Không được để trống Phone!!!")
        .min(10, "Số điện thoại không được ít hơn 10 số")
        .max(11, "Số điện thoại không được nhiều hơn 11 số")
        .matches(/^[0-9]+$/, "Số điện thoại không được nhập chữ, hay ký tự!!!"),
      birthday: Yup.string().required("Không được để trống Birthday!!!"),
      gender: Yup.string().required("Không được để trống giới tính"),
    }),
    onSubmit: (values) => {
      userServ
        .postRegister(values)
        .then((res) => {
          message.success("Đăng ký thành công");
          navigate("/login");
        })
        .catch((err) => {
          message.error("Đăng ký thất bại. Có thể bạn nhập chùng Email!!!");
        });
    },
  });

  const handleChangeDataPicker = (values) => {
    let birthday = moment(values).format("DD/MM/YYYY");
    formik.setFieldValue("birthday", birthday);
  };

  return (
    <div className="container mx-auto h-screen w-screen flex items-center justify-center  ">
      <div className="w-1/2 h-full ">
        <Lottie animationData={bg_animate} />
      </div>
      <div className="w-1/2 h-full flex items-center ">
        <Form
          className=" w-full "
          layout="vertical"
          onSubmitCapture={formik.handleSubmit}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 48,
          }}
        >
          <h1 className="text-5xl">Đăng Ký Fiverr</h1>
          <Form.Item label="Nhập Name">
            <Input name="name" onChange={formik.handleChange} />
            {formik.errors.name && (
              <p className="text-red-500">{formik.errors.name}</p>
            )}
          </Form.Item>

          <Form.Item label="Nhập Email">
            <Input type="email" name="email" onChange={formik.handleChange} />
            {formik.errors.email && (
              <p className="text-red-500">{formik.errors.email}</p>
            )}
          </Form.Item>

          <Form.Item label="Nhập PassWord">
            <Input name="password" onChange={formik.handleChange} />
            {formik.errors.password && (
              <p className="text-red-500">{formik.errors.password}</p>
            )}
          </Form.Item>

          <Form.Item label="Nhập Phone">
            <Input name="phone" onChange={formik.handleChange} />
            {formik.errors.phone && (
              <p className="text-red-500">{formik.errors.phone}</p>
            )}
          </Form.Item>

          <Form.Item label="Nhập Birthday">
            <DatePicker
              className="w-1/2"
              format={"DD/MM/YYYY"}
              onChange={handleChangeDataPicker}
            />
            {formik.errors.birthday && (
              <p className="text-red-500">{formik.errors.birthday}</p>
            )}
          </Form.Item>

          <Form.Item label="Chọn giới tính">
            <Radio.Group name="gender" onChange={formik.handleChange}>
              <Radio value="true"> Nam </Radio>
              <Radio value="false"> Nữ </Radio>
            </Radio.Group>
            {formik.errors.gender && (
              <p className="text-red-500">{formik.errors.gender}</p>
            )}
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" className="mr-5">
              Đăng Ký
            </Button>
            <NavLink to="/login">
              <Button type="link">Đăng Nhập</Button>
            </NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default SignupPage;

import React, { useEffect } from "react";
import { userServ } from "../../Services/userService";
import { useDispatch, useSelector } from "react-redux";
import UserAction from "./UserAction";
import { setListUser } from "../../Redux/actions/actionUser";

import { Button, Table, Tag } from "antd";
import { Input } from "antd";
import { NavLink } from "react-router-dom";

export default function QuanLyUser() {
  const { Search } = Input;

  let dispatch = useDispatch();

  const arrUsers = useSelector((state) => {
    return state.userReducer.listUser;
  });

  useEffect(() => {
    userServ
      .getListUser()
      .then((res) => {
        let dataUser = res.data.content.map((item) => {
          return { ...item, action: <UserAction item={item} /> };
        });
        dispatch(setListUser(dataUser));
      })
      .catch((err) => {});
  }, []);

  const columns = [
    {
      title: "Họ Tên",
      dataIndex: "name",
      key: "name",
      sorter: (a, b) => {
        let hoTenA = a.name.toLowerCase().trim();
        let hoTenB = b.name.toLowerCase().trim();
        if (hoTenA > hoTenB) {
          return 1;
        }
        return -1;
      },
      width: "10%",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      width: "15%",
    },
    {
      title: "Ngày sinh",
      dataIndex: "birthday",
      key: "birthday",
      width: "8%",
    },
    {
      title: "Giới tính",
      dataIndex: "gender",
      key: "gender",
      width: "8%",
      render: (text) => {
        if (text == true) {
          return <Tag color="yellow">Nam</Tag>;
        } else {
          return <Tag color="green">Nữ</Tag>;
        }
      },
    },
    {
      title: "Kỹ Năng",
      dataIndex: "skill",
      key: "skill",
      width: "11%",
    },
    {
      title: "Chứng nhận",
      dataIndex: "certification",
      key: "certification",
      width: "11%",
    },
    {
      title: "Công việc đặt trước",
      dataIndex: "bookingJob",
      key: "bookingJob",
      width: "11%",
    },
    {
      title: "Người dùng",
      dataIndex: "role",
      key: "role",
      width: "11%",
      render: (text) => {
        if (text == "ADMIN") {
          return <Tag color="red">Quản trị</Tag>;
        } else {
          return <Tag color="blue">Người dùng</Tag>;
        }
      },
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      width: "15%",
    },
  ];

  const onSearch = (value) => {
    if (value != "") {
      userServ
        .searchUser(value)
        .then((res) => {
          let dataUser = res.data.content.map((item) => {
            return { ...item, action: <UserAction item={item} /> };
          });
          dispatch(setListUser(dataUser));
        })
        .catch((err) => {});
    } else {
      userServ
        .getListUser()
        .then((res) => {
          let dataUser = res.data.content.map((item) => {
            return { ...item, action: <UserAction item={item} /> };
          });
          dispatch(setListUser(dataUser));
        })
        .catch((err) => {});
    }
  };

  const data = arrUsers;

  const onChange = (pagination, filters, sorter, extra) => {
    // console.log("params onChange Table", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h3 className="text-xl mb-5">Quản Lý người dùng</h3>
      <NavLink to="/admin/UserManage/AddUser">
        <Button type="primary" className="mb-5">
          Thêm người dùng
        </Button>
      </NavLink>
      <Search
        className="mb-5"
        placeholder="input search text"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey={"id"}
      />
    </div>
  );
}

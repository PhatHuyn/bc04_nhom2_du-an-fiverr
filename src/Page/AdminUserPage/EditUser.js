import { Button, Form, Input, Radio, Select, DatePicker } from "antd";
import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import moment from "moment";
import { userServ } from "../../Services/userService";

export default function EditUser() {
  const [componentSize, setComponentSize] = useState("default");
  const [ListUserEdit, setListUserEdit] = useState([]);
  const navigate = useNavigate();
  const { TextArea } = Input;
  let { id } = useParams();

  useEffect(() => {
    userServ
      .getListUserId(id)
      .then((res) => {
        let dataEditUser = res.data.content;
        setListUserEdit(dataEditUser);
      })
      .catch((err) => {});
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: id,
      name: ListUserEdit.name,
      email: ListUserEdit.email,
      phone: ListUserEdit.phone,
      birthday: ListUserEdit.birthday,
      gender: ListUserEdit.gender,
      role: ListUserEdit.role,
      skill: ListUserEdit.skill,
      certification: ListUserEdit.certification,
    },
    validationSchema: Yup.object({
      name: Yup.string().required("không được để trống Tên người dùng!!!"),
      email: Yup.string().required("Không được để trống Email!!!"),
      phone: Yup.string()
        .required("Không được để trống số điện thoại!!!")
        .min(10, "Số điện thoại không được ít hơn 10 số!!!")
        .max(11, "Số điện thoại không được nhiều hơn 11 số!!!")
        .matches(/^[0-9]+$/, "Số điện thoại không được nhập chữ, hay ký tự!!!"),
      role: Yup.string().required("Không được để trống Loại người dùng!!!"),
      gender: Yup.string().required("Không được để trống Giới tính!!!"),
      birthday: Yup.string().required("Không được để trống Ngày sinh!!!"),
    }),
    onSubmit: (values) => {
      userServ
        .EditUser(id, values)
        .then((res) => {
          alert("Cập nhật thành công");
          navigate("/admin/UserManage");
        })
        .catch((err) => {
          alert(err.response.data.content);
        });
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeLoaiNguoiDung = (value) => {
    formik.setFieldValue("role", value);
  };

  const handleChangeDataPicker = (values) => {
    let birthday = moment(values).format("DD/MM/YYYY");

    formik.setFieldValue("birthday", birthday);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Cập nhật thông tin Người dùng</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập tên người dùng">
        <Input
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
        />
        {formik.errors.name && (
          <p className="text-red-500">{formik.errors.name}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập email">
        <Input
          type="email"
          name="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        />
        {formik.errors.email && (
          <p className="text-red-500">{formik.errors.email}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập số điện thoại">
        <Input
          name="phone"
          onChange={formik.handleChange}
          value={formik.values.phone}
        />
        {formik.errors.phone && (
          <p className="text-red-500">{formik.errors.phone}</p>
        )}
      </Form.Item>

      <Form.Item label="Loại người dùng">
        <Select
          options={[
            { label: "Khách hàng", value: "USER" },
            { label: "Quản trị", value: "ADMIN" },
          ]}
          onChange={handleChangeLoaiNguoiDung}
          placeholder="Xin vui lòng chọn Loại người dùng"
          value={formik.values.role}
        />
        {formik.errors.role && (
          <p className="text-red-500">{formik.errors.role}</p>
        )}
      </Form.Item>

      <Form.Item label="Chọn giới tính">
        <Radio.Group
          name="gender"
          onChange={formik.handleChange}
          value={formik.values.gender}
        >
          <Radio value={true}> Nam </Radio>
          <Radio value={false}> Nữ </Radio>
        </Radio.Group>
        {formik.errors.gender && (
          <p className="text-red-500">{formik.errors.gender}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập ngày sinh">
        <DatePicker
          className="w-1/2"
          value={moment(formik.values.birthday, "DD/MM/YYYY")}
          format={"DD/MM/YYYY"}
          onChange={handleChangeDataPicker}
        />
        {formik.errors.birthday && (
          <p className="text-red-500">{formik.errors.birthday}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập Kỹ năng">
        <TextArea
          rows={3}
          name="skill"
          onChange={formik.handleChange}
          value={formik.values.skill}
          // placeholder="Vui lòng nhập kỹ năng nếu có"
          placeholder="Hiện đang bảo trì!!!"
          disabled
        />
      </Form.Item>

      <Form.Item label="Nhập chứng nhận">
        <TextArea
          rows={3}
          name="certification"
          onChange={formik.handleChange}
          value={formik.values.certification}
          // placeholder="Vui lòng nhập chứng nhận nếu có"
          placeholder="Hiện đang bảo trì!!!"
          disabled
        />
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Cập nhật Người Dùng
        </Button>
      </Form.Item>
    </Form>
  );
}

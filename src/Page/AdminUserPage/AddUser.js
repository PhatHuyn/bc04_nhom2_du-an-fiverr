import { Button, Form, Input, Radio, Select, DatePicker } from "antd";
import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import { userServ } from "../../Services/userService";

export default function AddUser() {
  const [componentSize, setComponentSize] = useState("default");
  const navigate = useNavigate();
  const { TextArea } = Input;

  const formik = useFormik({
    initialValues: {
      id: 0,
      name: "",
      email: "",
      password: "",
      phone: "",
      birthday: "",
      gender: "",
      role: "",
      skill: [],
      certification: [],
    },
    validationSchema: Yup.object({
      name: Yup.string().required("không được để trống Tên người dùng!!!"),
      email: Yup.string().required("Không được để trống Email!!!"),
      password: Yup.string().required("không được để trống Mật Khẩu!!!"),
      phone: Yup.string()
        .required("Không được để trống số điện thoại!!!")
        .min(10, "Số điện thoại không được ít hơn 10 số!!!")
        .max(11, "Số điện thoại không được nhiều hơn 11 số!!!")
        .matches(/^[0-9]+$/, "Số điện thoại không được nhập chữ, hay ký tự!!!"),
      role: Yup.string().required("Không được để trống Loại người dùng!!!"),
      gender: Yup.string().required("Không được để trống Giới tính!!!"),
      birthday: Yup.string().required("Không được để trống Ngày sinh!!!"),
    }),
    onSubmit: (values) => {
      userServ
        .addUser(values)
        .then((res) => {
          alert("Thêm User thành công");
          navigate("/admin/UserManage");
        })
        .catch((err) => {
          alert(err.response.data.content);
        });
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeLoaiNguoiDung = (value) => {
    formik.setFieldValue("role", value);
  };

  const handleChangeDataPicker = (values) => {
    let birthday = moment(values).format("DD/MM/YYYY");

    formik.setFieldValue("birthday", birthday);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Thêm mới Người dùng</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập tên người dùng">
        <Input name="name" onChange={formik.handleChange} />
        {formik.errors.name && (
          <p className="text-red-500">{formik.errors.name}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập email">
        <Input type="email" name="email" onChange={formik.handleChange} />
        {formik.errors.email && (
          <p className="text-red-500">{formik.errors.email}</p>
        )}
      </Form.Item>

      <Form.Item label="Mật khẩu">
        <Input name="password" onChange={formik.handleChange} />
        {formik.errors.password && (
          <p className="text-red-500">{formik.errors.password}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập số điện thoại">
        <Input name="phone" onChange={formik.handleChange} />
        {formik.errors.phone && (
          <p className="text-red-500">{formik.errors.phone}</p>
        )}
      </Form.Item>

      <Form.Item label="Loại người dùng">
        <Select
          options={[
            { label: "Khách hàng", value: "USER" },
            { label: "Quản trị", value: "ADMIN" },
          ]}
          onChange={handleChangeLoaiNguoiDung}
          placeholder="Xin vui lòng chọn Loại người dùng"
        />
        {formik.errors.role && (
          <p className="text-red-500">{formik.errors.role}</p>
        )}
      </Form.Item>

      <Form.Item label="Chọn giới tính">
        <Radio.Group name="gender" onChange={formik.handleChange}>
          <Radio value="true"> Nam </Radio>
          <Radio value="false"> Nữ </Radio>
        </Radio.Group>
        {formik.errors.gender && (
          <p className="text-red-500">{formik.errors.gender}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập ngày sinh">
        <DatePicker
          className="w-1/2"
          format={"DD/MM/YYYY"}
          onChange={handleChangeDataPicker}
        />
        {formik.errors.birthday && (
          <p className="text-red-500">{formik.errors.birthday}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập Kỹ năng">
        <TextArea
          rows={3}
          name="skill"
          onChange={formik.handleChange}
          // placeholder="Vui lòng nhập kỹ năng nếu có"
          placeholder="Hiện đang bảo trì!!!"
          disabled
        />
      </Form.Item>

      <Form.Item label="Nhập chứng nhận">
        <TextArea
          rows={3}
          name="certification"
          onChange={formik.handleChange}
          // placeholder="Vui lòng nhập chứng nhận nếu có"
          placeholder="Hiện đang bảo trì!!!"
          disabled
        />
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Thêm Người Dùng
        </Button>
      </Form.Item>
    </Form>
  );
}

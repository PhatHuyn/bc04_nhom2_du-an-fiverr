import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Table, Input } from "antd";
import { NavLink } from "react-router-dom";
import { typeofworkService } from "../../Services/typeofworkService";
import TypeOfWorkAction from "./TypeOfWorkAction";
import { setListTypeOfWork } from "../../Redux/actions/actionTypeOfWork";

export default function QuanLyTypeOfWork() {
  const { Search } = Input;
  let dispatch = useDispatch();

  const arrTypeOfWork = useSelector((state) => {
    return state.typeofworkReducer.listTypeOfWork;
  });

  useEffect(() => {
    typeofworkService
      .getListTypeOfWork()
      .then((res) => {
        let dataTypeOfWork = res.data.content.map((item) => {
          return { ...item, action: <TypeOfWorkAction item={item} /> };
        });
        dispatch(setListTypeOfWork(dataTypeOfWork));
      })
      .catch((err) => {});
  }, []);

  const columns = [
    {
      title: "Mã loại",
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => a.id - b.id,
    },
    {
      title: "Tên loại công việc",
      dataIndex: "tenLoaiCongViec",
      key: "tenLoaiCongViec",
      sorter: (a, b) => {
        let hoTenA = a.tenLoaiCongViec.toLowerCase().trim();
        let hoTenB = b.tenLoaiCongViec.toLowerCase().trim();
        if (hoTenA > hoTenB) {
          return 1;
        }
        return -1;
      },
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
    },
  ];

  const onSearch = (value) => {
    let datamap = [];
    if (value != "") {
      typeofworkService
        .searchTypeOfWorkId(value)
        .then((res) => {
          let datasearch = res.data.content;
          datamap.push(datasearch);
          let dataSearch2 = datamap.map((item) => {
            return { ...item, action: <TypeOfWorkAction item={item} /> };
          });

          dispatch(setListTypeOfWork(dataSearch2));
        })
        .catch((err) => {});
    } else {
      typeofworkService
        .getListTypeOfWork()
        .then((res) => {
          let dataTypeOfWork = res.data.content.map((item) => {
            return { ...item, action: <TypeOfWorkAction item={item} /> };
          });
          dispatch(setListTypeOfWork(dataTypeOfWork));
        })
        .catch((err) => {});
    }
  };

  const data = arrTypeOfWork;

  const onChange = (pagination, filters, sorter, extra) => {
    // console.log("params onChange Table", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h3 className="text-xl mb-5">Quản Lý loại công việc</h3>
      <NavLink to="/admin/TypeOfWorkManage/AddTypeOfWork">
        <Button type="primary" className="mb-5">
          Thêm loại công việc
        </Button>
      </NavLink>
      <Search
        className="mb-5"
        placeholder="input search number"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey={"id"}
      />
    </div>
  );
}

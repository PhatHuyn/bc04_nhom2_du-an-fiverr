import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { setListTypeOfWork } from "../../Redux/actions/actionTypeOfWork";
import { typeofworkService } from "../../Services/typeofworkService";

export default function TypeOfWorkAction({ item }) {
  let dispatch = useDispatch();
  return (
    <div>
      <NavLink to={`/admin/TypeOfWorkManage/EditTtpeOfWork/${item.id}`}>
        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2">
          Sửa
        </button>
      </NavLink>
      <button
        onClick={() => {
          if (
            window.confirm(
              `Bạn có chắc muốn xoá Loại công việc "${item.tenLoaiCongViec}"`
            )
          ) {
            typeofworkService
              .DeleteTypeOfWorkId(item.id)
              .then((res) => {
                alert("Xoá loại công việc thành Công!!!");
                typeofworkService
                  .getListTypeOfWork()
                  .then((res) => {
                    let dataTypeOfWork = res.data.content.map((item) => {
                      return {
                        ...item,
                        action: <TypeOfWorkAction item={item} />,
                      };
                    });
                    dispatch(setListTypeOfWork(dataTypeOfWork));
                  })
                  .catch((err) => {});
              })
              .catch((err) => {
                alert(err.response.data.content);
              });
          }
        }}
        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2"
      >
        Xoá
      </button>
    </div>
  );
}

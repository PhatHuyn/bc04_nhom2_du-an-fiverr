import { Button, Form, Input, Radio } from "antd";
import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import { typeofworkService } from "../../Services/typeofworkService";

export default function AddTypeOfWork() {
  const [componentSize, setComponentSize] = useState("default");
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      id: 0,
      tenLoaiCongViec: "",
    },
    validationSchema: Yup.object({
      tenLoaiCongViec: Yup.string().required(
        "không được để trống tên loại công việc!!!"
      ),
    }),
    onSubmit: (values) => {
      typeofworkService
        .AddTypeOfWork(values)
        .then((res) => {
          alert("thêm loại công việc thành công!!!");
          navigate("/admin/TypeOfWorkManage");
        })
        .catch((err) => {});
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Thêm mới Loại công Việc</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập tên loại công việc">
        <Input name="tenLoaiCongViec" onChange={formik.handleChange} />
        {formik.errors.tenLoaiCongViec && (
          <p className="text-red-500">{formik.errors.tenLoaiCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Thêm mới loại công việc
        </Button>
      </Form.Item>
    </Form>
  );
}

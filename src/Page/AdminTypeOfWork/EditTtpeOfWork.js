import { Button, Form, Input, Radio } from "antd";
import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { typeofworkService } from "../../Services/typeofworkService";

export default function EditTtpeOfWork() {
  const [componentSize, setComponentSize] = useState("default");
  const [dataEditTypeOfWork, setdataEditTypeOfWork] = useState([]);
  const navigate = useNavigate();
  let { id } = useParams();

  useEffect(() => {
    typeofworkService
      .searchTypeOfWorkId(id)
      .then((res) => {
        let data = res.data.content;
        setdataEditTypeOfWork(data);
      })
      .catch((err) => {});
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: 0,
      tenLoaiCongViec: dataEditTypeOfWork.tenLoaiCongViec,
    },
    validationSchema: Yup.object({
      tenLoaiCongViec: Yup.string().required(
        "không được để trống tên loại công việc!!!"
      ),
    }),
    onSubmit: (values) => {
      typeofworkService
        .UpdateTypeOfWork(id, values)
        .then((res) => {
          alert("Cập nhật loại công việc thành công!!!");
          navigate("/admin/TypeOfWorkManage");
        })
        .catch((err) => {});
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Cập nhật Loại công Việc</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập tên loại công việc">
        <Input
          name="tenLoaiCongViec"
          onChange={formik.handleChange}
          value={formik.values.tenLoaiCongViec}
        />
        {formik.errors.tenLoaiCongViec && (
          <p className="text-red-500">{formik.errors.tenLoaiCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Cập nhật loại công việc
        </Button>
      </Form.Item>
    </Form>
  );
}

import { Button, Form, Input, Radio, InputNumber } from "antd";
import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { workService } from "../../Services/workService";

export default function EditWork() {
  const [componentSize, setComponentSize] = useState("default");
  const [ArrEditWork, setArrEditWork] = useState([]);
  const navigate = useNavigate();
  const { TextArea } = Input;
  let { id } = useParams();

  useEffect(() => {
    workService
      .getWorkId(id)
      .then((res) => {
        var dataEdit = res.data.content;
        setArrEditWork(dataEdit);
      })
      .catch((err) => {});
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: ArrEditWork?.id,
      tenCongViec: ArrEditWork?.tenCongViec,
      danhGia: ArrEditWork?.danhGia,
      giaTien: ArrEditWork?.giaTien,
      nguoiTao: ArrEditWork?.nguoiTao,
      moTa: ArrEditWork?.moTa,
      maChiTietLoaiCongViec: ArrEditWork?.maChiTietLoaiCongViec,
      moTaNgan: ArrEditWork?.moTaNgan,
      saoCongViec: ArrEditWork?.saoCongViec,
      hinhAnh: "",
    },
    validationSchema: Yup.object({
      tenCongViec: Yup.string().required(
        "không được để trống tên công việc!!!"
      ),
      danhGia: Yup.string()
        .required("Không được để trống đánh giá!!!")
        .matches(/^[0-9]+$/, "Đánh giá không được nhập chữ, hay ký tự!!!"),
      giaTien: Yup.string()
        .required("không được để trống giá tiền!!!")
        .matches(/^[0-9]+$/, "Giá tiền không được nhập chữ, hay ký tự!!!"),
      nguoiTao: Yup.string()
        .required("Không được để trống người tạo!!!")
        .matches(/^[0-9]+$/, "Người tạo không được nhập chữ, hay ký tự!!!"),
      moTa: Yup.string().required("Không được để trống mô tả!!!"),
      maChiTietLoaiCongViec: Yup.string()
        .required("Không được để trống Mã chi tiết loại công việc!!!")
        .matches(
          /^[0-9]+$/,
          "Mã chi tiết loại công việc không được nhập chữ, hay ký tự!!!"
        ),
      moTaNgan: Yup.string().required("Không được để trống mô tả ngắn!!!"),
      saoCongViec: Yup.string()
        .required("Không được để trống sao công việc!!!")
        .matches(/^[0-9]+$/, "Sao công việc không được nhập chữ, hay ký tự!!!"),
    }),
    onSubmit: (values) => {
      workService
        .EditWork(id, values)
        .then((res) => {
          alert("Cập nhật thành công!!!");
          navigate("/admin/WorkManage");
        })
        .catch((err) => {
          alert(err.response.data.content);
        });
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeInputNumber = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Cập nhật Công Việc</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập tên công việc">
        <Input
          name="tenCongViec"
          onChange={formik.handleChange}
          value={formik.values.tenCongViec}
        />
        {formik.errors.tenCongViec && (
          <p className="text-red-500">{formik.errors.tenCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Mô tả">
        <TextArea
          rows={3}
          name="moTa"
          onChange={formik.handleChange}
          placeholder="Vui lòng nhập mô tả"
          value={formik.values.moTa}
        />
        {formik.errors.moTa && (
          <p className="text-red-500">{formik.errors.moTa}</p>
        )}
      </Form.Item>

      <Form.Item label="Mô tả ngắn">
        <TextArea
          rows={3}
          name="moTaNgan"
          onChange={formik.handleChange}
          placeholder="Vui lòng nhập mô tả ngắn"
          value={formik.values.moTaNgan}
        />
        {formik.errors.moTaNgan && (
          <p className="text-red-500">{formik.errors.moTaNgan}</p>
        )}
      </Form.Item>

      <Form.Item label="Người tạo">
        <InputNumber
          onChange={handleChangeInputNumber("nguoiTao")}
          value={formik.values.nguoiTao}
        />
        {formik.errors.nguoiTao && (
          <p className="text-red-500">{formik.errors.nguoiTao}</p>
        )}
      </Form.Item>

      <Form.Item label="Mã chi tiết loại công việc">
        <InputNumber
          onChange={handleChangeInputNumber("maChiTietLoaiCongViec")}
          value={formik.values.maChiTietLoaiCongViec}
        />
        {formik.errors.maChiTietLoaiCongViec && (
          <p className="text-red-500">{formik.errors.maChiTietLoaiCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Giá tiền">
        <InputNumber
          onChange={handleChangeInputNumber("giaTien")}
          value={formik.values.giaTien}
        />
        {formik.errors.giaTien && (
          <p className="text-red-500">{formik.errors.giaTien}</p>
        )}
      </Form.Item>

      <Form.Item label="Đánh giá">
        <InputNumber
          onChange={handleChangeInputNumber("danhGia")}
          min={0}
          max={100}
          value={formik.values.danhGia}
        />
        {formik.errors.danhGia && (
          <p className="text-red-500">{formik.errors.danhGia}</p>
        )}
      </Form.Item>

      <Form.Item label="Sao công việc">
        <InputNumber
          onChange={handleChangeInputNumber("saoCongViec")}
          min={0}
          max={5}
          value={formik.values.saoCongViec}
        />
        {formik.errors.saoCongViec && (
          <p className="text-red-500">{formik.errors.saoCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Cập Nhật Công Việc
        </Button>
      </Form.Item>
    </Form>
  );
}

import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Table, Input } from "antd";
import { NavLink } from "react-router-dom";
import { workService } from "../../Services/workService";
import WorkAction from "./WorkAction";
import { setListWork } from "../../Redux/actions/actionWork";

export default function QuanlyWork() {
  const { Search } = Input;
  let dispatch = useDispatch();

  const arrWork2 = useSelector((state) => {
    return state.workReducer.listWork;
  });

  useEffect(() => {
    workService
      .getListWork()
      .then((res) => {
        let dataWork = res.data.content.map((item) => {
          return { ...item, action: <WorkAction item={item} /> };
        });
        dispatch(setListWork(dataWork));
      })
      .catch((err) => {});
  }, []);

  const data = arrWork2;

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => a.id - b.id,
      width: "5%",
    },
    {
      title: "Tên Công Việc",
      dataIndex: "tenCongViec",
      key: "tenCongViec",
      sorter: (a, b) => {
        let hoTenA = a.tenCongViec.toLowerCase().trim();
        let hoTenB = b.tenCongViec.toLowerCase().trim();
        if (hoTenA > hoTenB) {
          return 1;
        }
        return -1;
      },
      width: "15%",
    },
    {
      title: "Hình Ảnh Công việc",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
      render: (text, work, index) => {
        return (
          <Fragment>
            <img
              src={work.hinhAnh}
              alt={work.tenCongViec}
              width={200}
              height={100}
              onError={(e) => {
                e.target.onError = null;
                e.target.src = `https://picsum.photos/id/${index}/50/50`;
              }}
            />
          </Fragment>
        );
      },
      width: "15%",
    },
    {
      title: "Mô tả ",
      dataIndex: "moTa",
      key: "moTa",
      render: (text, work) => {
        return (
          <Fragment>
            {work.moTa.length > 50
              ? work.moTa.substr(0, 50) + " ..."
              : work.moTa}
          </Fragment>
        );
      },
      width: "15%",
    },
    {
      title: "Giá Tiền",
      dataIndex: "giaTien",
      key: "giaTien",
      width: "8%",
      render: (text, work) => {
        return <Fragment>{`${work.giaTien} $`}</Fragment>;
      },
    },
    {
      title: "Đánh giá ",
      dataIndex: "danhGia",
      key: "danhGia",
      width: "8%",
    },
    {
      title: "Mô tả ",
      dataIndex: "moTaNgan",
      key: "moTaNgan",
      render: (text, work) => {
        return (
          <Fragment>
            {work.moTaNgan.length > 50
              ? work.moTaNgan.substr(0, 50) + " ..."
              : work.moTaNgan}
          </Fragment>
        );
      },
      width: "15%",
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      width: "15%",
    },
  ];

  const onSearch = (value) => {
    if (value != "") {
      workService
        .sreachWork(value)
        .then((res) => {
          let arrSearch = [];
          let dataSreach = res.data.content;
          for (let i = 0; i <= dataSreach.length - 1; i++) {
            let datai = dataSreach[i].congViec;
            arrSearch.push(datai);
            let datamap = arrSearch.map((item) => {
              return { ...item, action: <WorkAction item={item} /> };
            });
            dispatch(setListWork(datamap));
          }
        })
        .catch((err) => {});
    } else {
      workService
        .getListWork()
        .then((res) => {
          let dataWork = res.data.content.map((item) => {
            return { ...item, action: <WorkAction item={item} /> };
          });
          dispatch(setListWork(dataWork));
        })
        .catch((err) => {});
    }
  };

  const onChange = (pagination, filters, sorter, extra) => {
    // console.log("params onChange Table", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h3 className="text-xl mb-5">Quản Lý công việc</h3>
      <NavLink to="/admin/WorkManage/AddWork">
        <Button type="primary" className="mb-5">
          Thêm công việc
        </Button>
      </NavLink>
      <Search
        className="mb-5"
        placeholder="input search text"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey={"id"}
      />
    </div>
  );
}

import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { setListWork } from "../../Redux/actions/actionWork";
import { workService } from "../../Services/workService";

export default function WorkAction({ item }) {
  let dispatch = useDispatch();

  return (
    <div>
      <NavLink to={`/admin/WorkManage/EditWork/${item.id}`}>
        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2">
          Sửa
        </button>
      </NavLink>
      <button
        onClick={() => {
          if (
            window.confirm(`Bạn có chắc muốn xoá Công việc thứ "${item.id}"`)
          ) {
            workService
              .DeleteWork(item.id)
              .then((res) => {
                workService
                  .getListWork()
                  .then((res) => {
                    let dataWork = res.data.content.map((item) => {
                      return { ...item, action: <WorkAction item={item} /> };
                    });
                    dispatch(setListWork(dataWork));
                  })
                  .catch((err) => {});
                alert("Xoá Công việc thành Công!!!");
              })
              .catch((err) => {
                alert(err.response.data.content);
              });
          }
        }}
        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2"
      >
        Xoá
      </button>
    </div>
  );
}

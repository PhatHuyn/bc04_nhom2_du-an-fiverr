import { Button, Form, Input, Radio, DatePicker, Switch } from "antd";
import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import moment from "moment";
import { hireajobService } from "../../Services/hireajobService";

export default function EditHireAJob() {
  const [componentSize, setComponentSize] = useState("default");
  const [dataEdit, setdataEdit] = useState([]);
  const navigate = useNavigate();
  let { id } = useParams();

  useEffect(() => {
    hireajobService
      .SearchHireAJobId(id)
      .then((res) => {
        var dataEdit = res.data.content;

        setdataEdit(dataEdit);
      })
      .catch((err) => {});
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: 0,
      maCongViec: dataEdit.maCongViec,
      maNguoiThue: dataEdit.maNguoiThue,
      ngayThue: dataEdit.ngayThue,
      hoanThanh: dataEdit.hoanThanh,
    },
    validationSchema: Yup.object({
      maCongViec: Yup.string()
        .required("Không được để trống mã công việc!!!")
        .matches(/^[0-9]+$/, "Mã công việc không được nhập chữ, hay ký tự!!!"),
      ngayThue: Yup.string().required("không được để trống Ngày Thuê!!!"),
      maNguoiThue: Yup.string()
        .required("Không được để trống mã người thuê!!!")
        .matches(/^[0-9]+$/, "Mã người thuw không được nhập chữ, hay ký tự!!!"),
      hoanThanh: Yup.string().required("Xin hãy chọn hoàn thành!!!"),
    }),
    onSubmit: (values) => {
      hireajobService
        .EditHireAJob(id, values)
        .then((res) => {
          alert("Cập nhật thành công!!!");
          navigate("/admin/HireAJobManage");
        })
        .catch((err) => {
          alert(err.response.data.content);
        });
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleChangeDataPicker = (values) => {
    let ngayThue = moment(values).format("DD/MM/YYYY");
    formik.setFieldValue("ngayThue", ngayThue);
  };

  const handleChangeSwitch = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Cập nhật Thuê Công Việc</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập mã công việc">
        <Input
          name="maCongViec"
          onChange={formik.handleChange}
          value={formik.values.maCongViec}
        />
        {formik.errors.maCongViec && (
          <p className="text-red-500">{formik.errors.maCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập mã người thuê">
        <Input
          name="maNguoiThue"
          onChange={formik.handleChange}
          value={formik.values.maNguoiThue}
        />
        {formik.errors.maNguoiThue && (
          <p className="text-red-500">{formik.errors.maNguoiThue}</p>
        )}
      </Form.Item>

      <Form.Item label="Nhập ngày Thuê">
        <DatePicker
          className="w-1/2"
          format={"DD/MM/YYYY"}
          onChange={handleChangeDataPicker}
          value={moment(formik.values.ngayThue, "DD/MM/YYYY")}
        />
        {formik.errors.ngayThue && (
          <p className="text-red-500">{formik.errors.ngayThue}</p>
        )}
      </Form.Item>

      <Form.Item label="Hoàn thành" valuePropName="checked">
        <Switch
          onChange={handleChangeSwitch("hoanThanh")}
          checked={formik.values.hoanThanh}
        />
        {formik.errors.hoanThanh && (
          <p className="text-red-500">{formik.errors.hoanThanh}</p>
        )}
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Cập nhật
        </Button>
      </Form.Item>
    </Form>
  );
}

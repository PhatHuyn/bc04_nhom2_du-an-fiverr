import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Table, Input, Tag } from "antd";
import { NavLink } from "react-router-dom";
import { hireajobService } from "../../Services/hireajobService";
import HireAJobAction from "./HireAJobAction";
import { setListHireAJob } from "../../Redux/actions/actionHireAJob";

export default function QuanLyHireAJobPage() {
  const { Search } = Input;
  let dispatch = useDispatch();

  const arrHireAJobWork = useSelector((state) => {
    return state.hireajobReducer.listHireAJob;
  });

  useEffect(() => {
    hireajobService
      .getListHireAJob()
      .then((res) => {
        let dataWork = res.data.content.map((item) => {
          return { ...item, action: <HireAJobAction item={item} /> };
        });
        dispatch(setListHireAJob(dataWork));
      })
      .catch((err) => {});
  }, []);

  const data = arrHireAJobWork;

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => a.id - b.id,
      width: "5%",
    },
    {
      title: "Mã công việc",
      dataIndex: "maCongViec",
      key: "maCongViec",
      sorter: (a, b) => a.maCongViec - b.maCongViec,
      width: "5%",
    },
    {
      title: "Mã người thuê",
      dataIndex: "maNguoiThue",
      key: "maNguoiThue",
      sorter: (a, b) => a.maNguoiThue - b.maNguoiThue,
      width: "5%",
    },
    {
      title: "Hoàn thành",
      dataIndex: "hoanThanh",
      key: "hoanThanh",
      render: (text) => {
        if (text == true) {
          return <Tag color="yellow">Đã hoàn thành</Tag>;
        } else {
          return <Tag color="red">Chưa hoàn thành</Tag>;
        }
      },
      width: "20%",
    },
    {
      title: "Ngày Thuê",
      dataIndex: "ngayThue",
      key: "ngayThue",
      width: "15%",
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      width: "20%",
    },
  ];

  const onSearch = (value) => {
    if (value != "") {
      hireajobService
        .SearchHireAJobId(value)
        .then((res) => {
          let arrSearch = [];
          let dataSreach = res.data.content;
          arrSearch.push(dataSreach);
          let datamap = arrSearch.map((item) => {
            return { ...item, action: <HireAJobAction item={item} /> };
          });
          dispatch(setListHireAJob(datamap));
        })
        .catch((err) => {
          alert(err.response.data.message);
        });
    } else {
      hireajobService
        .getListHireAJob()
        .then((res) => {
          let dataWork = res.data.content.map((item) => {
            return { ...item, action: <HireAJobAction item={item} /> };
          });
          dispatch(setListHireAJob(dataWork));
        })
        .catch((err) => {});
    }
  };

  const onChange = (pagination, filters, sorter, extra) => {
    // console.log("params onChange Table", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h3 className="text-xl mb-5">Quản Lý Thuê công việc</h3>
      <NavLink to="/admin/HireAJobManage/AddHireAJob">
        <Button type="primary" className="mb-5">
          Thêm thuê công việc
        </Button>
      </NavLink>
      <Search
        className="mb-5"
        placeholder="input search number"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey={"id"}
      />
    </div>
  );
}

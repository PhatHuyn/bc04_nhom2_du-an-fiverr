import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { setListHireAJob } from "../../Redux/actions/actionHireAJob";
import { hireajobService } from "../../Services/hireajobService";

export default function HireAJobAction({ item }) {
  let dispatch = useDispatch();
  return (
    <div>
      <NavLink to={`/admin/HireAJobManage/EditHireAJob/${item.id}`}>
        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2">
          Sửa
        </button>
      </NavLink>
      <button
        onClick={() => {
          if (
            window.confirm(
              `Bạn có chắc muốn xoá Thuê công việc thứ "${item.id}"`
            )
          ) {
            hireajobService
              .DeleteHireAJob(item.id)
              .then((res) => {
                alert(`Thuê công việc thứ ${item.id} xoá thành Công!!!`);
                hireajobService
                  .getListHireAJob()
                  .then((res) => {
                    let dataWork = res.data.content.map((item) => {
                      return {
                        ...item,
                        action: <HireAJobAction item={item} />,
                      };
                    });
                    dispatch(setListHireAJob(dataWork));
                  })
                  .catch((err) => {});
              })
              .catch((err) => {
                alert(err.response.data.content);
              });
          }
        }}
        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2"
      >
        Xoá
      </button>
    </div>
  );
}

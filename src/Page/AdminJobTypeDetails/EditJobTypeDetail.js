import { Button, Form, Input, Radio } from "antd";
import React, { useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate, useParams } from "react-router-dom";
import { jobtypedetailService } from "../../Services/jobtypedetailService";

export default function EditJobTypeDetail() {
  const [componentSize, setComponentSize] = useState("default");
  const [arrEdit, setarrEdit] = useState([]);
  const navigate = useNavigate();
  const { TextArea } = Input;
  let { id } = useParams();

  useEffect(() => {
    jobtypedetailService
      .searchJobTypeDetailId(id)
      .then((res) => {
        var dataEdit = res.data.content;

        setarrEdit(dataEdit);
      })
      .catch((err) => {});
  }, []);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: 0,
      tenChiTiet: arrEdit?.tenNhom,
      maLoaiCongViec: arrEdit?.maLoaiCongviec,
      danhSachChiTiet: [],
    },
    validationSchema: Yup.object({
      tenChiTiet: Yup.string().required("không được để trống tên chi tiêt!!!"),
      maLoaiCongViec: Yup.string()
        .required("Không được để trống mã loại công việc!!!")
        .matches(
          /^[0-9]+$/,
          "Mã loại công việc không được nhập chữ, hay ký tự!!!"
        ),
      //   danhSachChiTiet: Yup.string()
      //     .required("Không được để trống danh sách chi tiết!!!")
      //     .matches(
      //       /^[0-9]+$/,
      //       "Danh sach chi tiết không được nhập chữ, hay ký tự!!!"
      //     ),
    }),

    onSubmit: (values) => {
      jobtypedetailService
        .EditJobTypeDetail(id, values)
        .then((res) => {
          navigate("/admin/JobTypeDetailManage");
          alert("Cập nhật Nhóm loại công việc thành công!!!");
        })
        .catch((err) => {
          alert(err.response.data.content);
        });
    },
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  return (
    <Form
      onSubmitCapture={formik.handleSubmit}
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: componentSize,
      }}
      onValuesChange={onFormLayoutChange}
      size={componentSize}
    >
      <h3 className="text-xl mb-5 ml-10">Cập nhật chi tiết loại</h3>
      <Form.Item label="Form Size" name="size">
        <Radio.Group>
          <Radio.Button value="small">Small</Radio.Button>
          <Radio.Button value="default">Default</Radio.Button>
          <Radio.Button value="large">Large</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item label="Nhập tên chi tiết">
        <Input
          name="tenChiTiet"
          onChange={formik.handleChange}
          value={formik.values.tenChiTiet}
        />
        {formik.errors.tenChiTiet && (
          <p className="text-red-500">{formik.errors.tenChiTiet}</p>
        )}
      </Form.Item>

      <Form.Item label="Mã loại công việc">
        <Input
          name="maLoaiCongViec"
          onChange={formik.handleChange}
          value={formik.values.maLoaiCongViec}
        />
        {formik.errors.maLoaiCongViec && (
          <p className="text-red-500">{formik.errors.maLoaiCongViec}</p>
        )}
      </Form.Item>

      <Form.Item label="Danh sách chi tiết">
        <TextArea
          rows={3}
          name="danhSachChiTiet"
          onChange={formik.handleChange}
          //   placeholder="Vui lòng nhập danh sách chi tiết"
          disabled
          placeholder="Hiện đang bảo trì!!!"
        />
        {/* {formik.errors.danhSachChiTiet && (
          <p className="text-red-500">{formik.errors.danhSachChiTiet}</p>
        )} */}
      </Form.Item>

      <Form.Item label="Chức năng">
        <Button type="primary" htmlType="submit">
          Cập nhật
        </Button>
      </Form.Item>
    </Form>
  );
}

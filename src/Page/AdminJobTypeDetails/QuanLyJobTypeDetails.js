import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Space, Table, Input } from "antd";
import { NavLink } from "react-router-dom";
import { jobtypedetailService } from "../../Services/jobtypedetailService";
import JobTypeDetailAction from "./JobTypeDetailAction";
import { setListJobTypeDetail } from "../../Redux/actions/actionJobTypeDetail";

export default function QuanLyJobTypeDetails() {
  const { Search } = Input;
  let dispatch = useDispatch();

  const arrJobTypeDetail = useSelector((state) => {
    return state.jobtypedetailReducer.listJobTypeDetail;
  });

  useEffect(() => {
    jobtypedetailService
      .getListJobTypeDetail()
      .then((res) => {
        let datajob = res.data.content.map((item) => {
          return { ...item, action: <JobTypeDetailAction item={item} /> };
        });
        dispatch(setListJobTypeDetail(datajob));
      })
      .catch((err) => {});
  }, []);

  const data = arrJobTypeDetail;

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      sorter: (a, b) => a.id - b.id,
      width: "5%",
    },
    {
      title: "Tên Nhóm",
      dataIndex: "tenNhom",
      key: "tenNhom",
      sorter: (a, b) => {
        let hoTenA = a.tenNhom.toLowerCase().trim();
        let hoTenB = b.tenNhom.toLowerCase().trim();
        if (hoTenA > hoTenB) {
          return 1;
        }
        return -1;
      },
      width: "15%",
    },
    {
      title: "Hình Ảnh Công việc",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
      render: (text, work, index) => {
        return (
          <Fragment>
            <img
              src={work.hinhAnh}
              alt={work.tenCongViec}
              width={250}
              height={150}
              onError={(e) => {
                e.target.onError = null;
                e.target.src = `https://picsum.photos/id/${index}/50/50`;
              }}
            />
          </Fragment>
        );
      },
      width: "20%",
    },
    {
      title: "Mã loại công việc",
      dataIndex: "maLoaiCongviec",
      key: "maLoaiCongviec",
      sorter: (a, b) => a.maLoaiCongviec - b.maLoaiCongviec,
      width: "8%",
    },
    {
      title: "Danh sách Chi tiết loại",
      dataIndex: "dsChiTietLoai",
      key: "dsChiTietLoai",
      render: (_, any) => (
        <Space size="middle">
          {any.dsChiTietLoai.map((item, i) => {
            return (
              <span key={i}>
                {item.id}.{item.tenChiTiet}
              </span>
            );
          })}
        </Space>
      ),
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      width: "15%",
    },
  ];

  const onSearch = (value) => {
    if (value != "") {
      jobtypedetailService
        .searchJobTypeDetailId(value)
        .then((res) => {
          let arrSearch = [];
          let dataSreach = res.data.content;
          arrSearch.push(dataSreach);
          let datajob = arrSearch.map((item) => {
            return { ...item, action: <JobTypeDetailAction item={item} /> };
          });
          dispatch(setListJobTypeDetail(datajob));
        })
        .catch((err) => {
          alert("Không tìm thấy");
        });
    } else {
      jobtypedetailService
        .getListJobTypeDetail()
        .then((res) => {
          let datajob = res.data.content.map((item) => {
            return { ...item, action: <JobTypeDetailAction item={item} /> };
          });
          dispatch(setListJobTypeDetail(datajob));
        })
        .catch((err) => {});
    }
  };

  const onChange = (pagination, filters, sorter, extra) => {
    // console.log("params onChange Table", pagination, filters, sorter, extra);
  };

  return (
    <div>
      <h3 className="text-xl mb-5">Quản Lý Chi tiết loại công việc</h3>
      <NavLink to="/admin/JobTypeDetailManage/AddJobTypeDetail">
        <Button type="primary" className="mb-5">
          Thêm Nhóm chi tiết loại
        </Button>
      </NavLink>
      <Search
        className="mb-5"
        placeholder="input search text"
        allowClear
        enterButton="Search"
        size="large"
        onSearch={onSearch}
      />
      <Table
        columns={columns}
        dataSource={data}
        onChange={onChange}
        rowKey={"id"}
      />
    </div>
  );
}

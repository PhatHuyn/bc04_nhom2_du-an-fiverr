import React from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { setListJobTypeDetail } from "../../Redux/actions/actionJobTypeDetail";
import { jobtypedetailService } from "../../Services/jobtypedetailService";

export default function JobTypeDetailAction({ item }) {
  let dispatch = useDispatch();
  return (
    <div>
      <NavLink to={`/admin/JobTypeDetailManage/EditJobTypeDetail/${item.id}`}>
        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2">
          Sửa
        </button>
      </NavLink>
      <button
        onClick={() => {
          if (
            window.confirm(
              `Bạn có chắc muốn xoá Nhóm chi tiết Loại thứ "${item.id}"`
            )
          ) {
            jobtypedetailService
              .DeleteJobTypeDetail(item.id)
              .then((res) => {
                alert(`Xoá Nhóm chi tiết thứ "${item.id}" thành công!!!`);
                jobtypedetailService
                  .getListJobTypeDetail()
                  .then((res) => {
                    let datajob = res.data.content.map((item) => {
                      return {
                        ...item,
                        action: <JobTypeDetailAction item={item} />,
                      };
                    });
                    dispatch(setListJobTypeDetail(datajob));
                  })
                  .catch((err) => {});
              })
              .catch((err) => {
                alert(err.response.data.content);
              });
          }
        }}
        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded mb-2 mr-2"
      >
        Xoá
      </button>
    </div>
  );
}

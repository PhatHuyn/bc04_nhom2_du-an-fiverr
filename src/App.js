import logo from "./logo.svg";
import "./App.css";
import "antd/dist/antd.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LayoutLogin from "./HOC/LayoutLogin";
import LoginPage from "./Page/LoginPage/LoginPage";
import SignupPage from "./Page/SignupPage/SignupPage";
import SecureView from "./HOC/SecureView";
import LayoutAdmin from "./HOC/LayoutAdmin";
import QuanLyUser from "./Page/AdminUserPage/QuanLyUser";
import AddUser from "./Page/AdminUserPage/AddUser";
import EditUser from "./Page/AdminUserPage/EditUser";
import QuanlyWork from "./Page/AdminWorkPage/QuanlyWork";
import AddWork from "./Page/AdminWorkPage/AddWork";
import EditWork from "./Page/AdminWorkPage/EditWork";
import QuanLyTypeOfWork from "./Page/AdminTypeOfWork/QuanLyTypeOfWork";
import AddTypeOfWork from "./Page/AdminTypeOfWork/AddTypeOfWork";
import EditTtpeOfWork from "./Page/AdminTypeOfWork/EditTtpeOfWork";
import QuanLyJobTypeDetails from "./Page/AdminJobTypeDetails/QuanLyJobTypeDetails";
import QuanLyHireAJobPage from "./Page/AdminHireAJobPage/QuanLyHireAJobPage";
import AddHireAJob from "./Page/AdminHireAJobPage/AddHireAJob";
import EditHireAJob from "./Page/AdminHireAJobPage/EditHireAJob";
import QuanLyComment from "./Page/AdminComment/QuanLyComment";
import AddComment from "./Page/AdminComment/AddComment";
import EditComment from "./Page/AdminComment/EditComment";
import AddJobTypeDetail from "./Page/AdminJobTypeDetails/AddJobTypeDetail";
import EditJobTypeDetail from "./Page/AdminJobTypeDetails/EditJobTypeDetail";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" exact element={<LayoutLogin Component={LoginPage} />} />
        <Route
          path="/login"
          exact
          element={<LayoutLogin Component={LoginPage} />}
        />
        <Route
          path="/signup"
          element={<LayoutLogin Component={SignupPage} />}
        />

        <Route
          path="/admin"
          element={
            <SecureView>
              <LayoutAdmin Component={QuanLyUser} />
            </SecureView>
          }
        />
        <Route
          path="/admin/UserManage"
          element={
            <SecureView>
              <LayoutAdmin Component={QuanLyUser} />
            </SecureView>
          }
        />
        <Route
          path="/admin/UserManage/AddUser"
          element={
            <SecureView>
              <LayoutAdmin Component={AddUser} />
            </SecureView>
          }
        />
        <Route
          path="/admin/UserManage/EditUser/:id"
          element={
            <SecureView>
              <LayoutAdmin Component={EditUser} />
            </SecureView>
          }
        />

        <Route
          path="/admin/WorkManage"
          element={
            <SecureView>
              <LayoutAdmin Component={QuanlyWork} />
            </SecureView>
          }
        />
        <Route
          path="/admin/WorkManage/AddWork"
          element={
            <SecureView>
              <LayoutAdmin Component={AddWork} />
            </SecureView>
          }
        />
        <Route
          path="/admin/WorkManage/EditWork/:id"
          element={
            <SecureView>
              <LayoutAdmin Component={EditWork} />
            </SecureView>
          }
        />

        <Route
          path="/admin/TypeOfWorkManage"
          element={
            <SecureView>
              <LayoutAdmin Component={QuanLyTypeOfWork} />
            </SecureView>
          }
        />
        <Route
          path="/admin/TypeOfWorkManage/AddTypeOfWork"
          element={
            <SecureView>
              <LayoutAdmin Component={AddTypeOfWork} />
            </SecureView>
          }
        />
        <Route
          path="/admin/TypeOfWorkManage/EditTtpeOfWork/:id"
          element={
            <SecureView>
              <LayoutAdmin Component={EditTtpeOfWork} />
            </SecureView>
          }
        />

        <Route
          path="/admin/JobTypeDetailManage"
          element={
            <SecureView>
              <LayoutAdmin Component={QuanLyJobTypeDetails} />
            </SecureView>
          }
        />
        <Route
          path="/admin/JobTypeDetailManage/AddJobTypeDetail"
          element={
            <SecureView>
              <LayoutAdmin Component={AddJobTypeDetail} />
            </SecureView>
          }
        />
        <Route
          path="/admin/JobTypeDetailManage/EditJobTypeDetail/:id"
          element={
            <SecureView>
              <LayoutAdmin Component={EditJobTypeDetail} />
            </SecureView>
          }
        />

        <Route
          path="/admin/HireAJobManage"
          element={
            <SecureView>
              <LayoutAdmin Component={QuanLyHireAJobPage} />
            </SecureView>
          }
        />
        <Route
          path="/admin/HireAJobManage/AddHireAJob"
          element={
            <SecureView>
              <LayoutAdmin Component={AddHireAJob} />
            </SecureView>
          }
        />
        <Route
          path="/admin/HireAJobManage/EditHireAJob/:id"
          element={
            <SecureView>
              <LayoutAdmin Component={EditHireAJob} />
            </SecureView>
          }
        />

        <Route
          path="/admin/CommentManage"
          element={
            <SecureView>
              <LayoutAdmin Component={QuanLyComment} />
            </SecureView>
          }
        />
        <Route
          path="/admin/CommentManage/AddComment"
          element={
            <SecureView>
              <LayoutAdmin Component={AddComment} />
            </SecureView>
          }
        />
        <Route
          path="/admin/CommentManage/EditComment/:id"
          element={
            <SecureView>
              <LayoutAdmin Component={EditComment} />
            </SecureView>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

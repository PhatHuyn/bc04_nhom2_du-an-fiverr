import { https } from "./configURL";

export const workService = {
  getListWork: () => {
    let uri = `/api/cong-viec`;
    return https.get(uri);
  },
  getWorkId: (id) => {
    let uri = `/api/cong-viec/${id}`;
    return https.get(uri);
  },
  sreachWork: (nameWork) => {
    let uri = `/api/cong-viec/lay-danh-sach-cong-viec-theo-ten/${nameWork}`;
    return https.get(uri);
  },
  AddWork: (dataWork) => {
    let uri = `/api/cong-viec`;
    return https.post(uri, dataWork);
  },
  EditWork: (id, data) => {
    let uri = `/api/cong-viec/${id}`;
    return https.put(uri, data);
  },
  DeleteWork: (id) => {
    let uri = `/api/cong-viec/${id}`;
    return https.delete(uri);
  },
};

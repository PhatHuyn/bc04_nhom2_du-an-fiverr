import { https } from "./configURL";

export const hireajobService = {
  getListHireAJob: () => {
    let uri = `/api/thue-cong-viec`;
    return https.get(uri);
  },
  SearchHireAJobId: (id) => {
    let uri = `/api/thue-cong-viec/${id}`;
    return https.get(uri);
  },
  AddHireAJob: (data) => {
    let uri = `/api/thue-cong-viec`;
    return https.post(uri, data);
  },
  EditHireAJob: (id, data) => {
    let uri = `/api/thue-cong-viec/${id}`;
    return https.put(uri, data);
  },
  DeleteHireAJob: (id) => {
    let uri = `/api/thue-cong-viec/${id}`;
    return https.delete(uri);
  },
};

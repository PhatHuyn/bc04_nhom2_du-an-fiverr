import { https } from "./configURL";

export const jobtypedetailService = {
  getListJobTypeDetail: () => {
    let uri = `/api/chi-tiet-loai-cong-viec`;
    return https.get(uri);
  },
  searchJobTypeDetailId: (id) => {
    let uri = `/api/chi-tiet-loai-cong-viec/${id}`;
    return https.get(uri);
  },
  AddJobTypeDetail: (data) => {
    let uri = `/api/chi-tiet-loai-cong-viec/them-nhom-chi-tiet-loai`;
    return https.post(uri, data);
  },
  DeleteJobTypeDetail: (id) => {
    let uri = `/api/chi-tiet-loai-cong-viec/${id}`;
    return https.delete(uri);
  },
  EditJobTypeDetail: (id, data) => {
    let uri = `/api/chi-tiet-loai-cong-viec/sua-nhom-chi-tiet-loai/${id}`;
    return https.put(uri, data);
  },
};

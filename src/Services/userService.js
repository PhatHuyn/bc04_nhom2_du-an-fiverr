import { https } from "./configURL";

export const userServ = {
  postLogin: (data) => {
    return https.post("/api/auth/signin", data);
  },
  postRegister: (data) => {
    return https.post("/api/auth/signup", data);
  },
  getListUser: () => {
    let uri = `/api/users`;
    return https.get(uri);
  },
  getListUserId: (id) => {
    let uri = `/api/users/${id}`;
    return https.get(uri);
  },
  searchUser: (searchName) => {
    let uri = `/api/users/search/${searchName}`;
    return https.get(uri);
  },
  addUser: (data) => {
    return https.post("/api/users", data);
  },
  EditUser: (id, data) => {
    return https.put(`/api/users/${id}`, data);
  },
  DeleteUser: (id) => {
    return https.delete(`/api/users?id=${id}`);
  },
};

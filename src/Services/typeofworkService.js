import { https } from "./configURL";

export const typeofworkService = {
  getListTypeOfWork: () => {
    let uri = `/api/loai-cong-viec`;
    return https.get(uri);
  },
  searchTypeOfWorkId: (id) => {
    let uri = `/api/loai-cong-viec/${id}`;
    return https.get(uri);
  },
  AddTypeOfWork: (data) => {
    let uri = `/api/loai-cong-viec`;
    return https.post(uri, data);
  },
  UpdateTypeOfWork: (id, data) => {
    let uri = `/api/loai-cong-viec/${id}`;
    return https.put(uri, data);
  },
  DeleteTypeOfWorkId: (id) => {
    let uri = `/api/loai-cong-viec/${id}`;
    return https.delete(uri);
  },
};

import { https } from "./configURL";

export const commentService = {
  getListComment: () => {
    let uri = `/api/binh-luan`;
    return https.get(uri);
  },
  SearchCommentId: (id) => {
    let uri = `/api/binh-luan/lay-binh-luan-theo-cong-viec/${id}`;
    return https.get(uri);
  },
  AddComment: (data) => {
    let uri = `/api/binh-luan`;
    return https.post(uri, data);
  },
  EditComment: (id, data) => {
    let uri = `/api/binh-luan/${id}`;
    return https.put(uri, data);
  },
  DeleteComment: (id) => {
    let uri = `/api/binh-luan/${id}`;
    return https.delete(uri);
  },
};

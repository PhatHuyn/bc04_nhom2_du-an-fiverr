# Fiverr project

Welcome to the Fiverr website. This is a project programmed by two web developers. First, I would like to introduce web developer Vo Ngoc Chien, who did the external layout of the website. And the other is Huynh Duc Phat who built the AdminPage function for the website. Website Fiverr is a website that helps you find Vietnamese jobs as well as hire other people to work for your company.
Thanks for watching!!!

## Task Table

https://docs.google.com/spreadsheets/d/1BYljBD_3p6wCcgeSnONKUbpk5ogmk-Kr04q2rviYkOs/edit#gid=0

## WireFrames

https://drive.google.com/file/d/1xgVrrLvl0ZzQ1j9Nz25CTsrBTPfkGdlL/view?usp=sharing

## Deployment

https://bc04-nhom2-du-an-fiverr.vercel.app/

## Requests

Create an account or you can use : userAccount , adminAccount

You cannot access the site AdminPage without an admin account.

## source code

1.(https://gitlab.com/PhatHuyn/bc04_nhom2_du-an-fiverr) đây là soure chạy deployment

2.(https://gitlab.com/rw14/bc04_nhom2_fiverrclone/-/tree/Phat/AdminPage) đây làm source nhóm làm chung của em và anh Chiến.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
